package com.mitocode.service;

import java.util.List;

public interface ICRUD<T> {
	
	T registrar(T t);
	
	T modificar(T t);
	
	void eliminar(Integer id);
	
	T listarId(Integer id);
	
	List<T> listar();
	
}
